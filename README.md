# FastDFS_deploy

#### 介绍
FastDFS在线一键安装

#### 软件架构
软件架构说明


#### 安装教程

1. ./one_key_install_FastDFS.sh

#### 使用说明

1. FastDFS_v5.05.tar.gz
2. fastdfs-nginx-module_v1.16.tar.gz
3. libfastcommon-master.zip
4. nginx-1.8.0.tar.gz

#### 测试

1. `/usr/bin/fdfs_test /etc/fdfs/client.conf upload /opt/download/fsp.jpg `


#### 删除脚本说明
1、修改FILE_PATH目录，选择到data目录，例如：/opt/fastdfs/fastdfs_storage_data/data
2、执行脚本 ./delete_file.sh
