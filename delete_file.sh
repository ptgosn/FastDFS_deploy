#!/bin/bash

FILE_PATH=/opt/xxx/xxx/tmp
CUR_TIME=`date +%s`
LAST_TIME=`expr $CUR_TIME - 86400`

function get_all_files(){
    filelist=`ls ${FILE_PATH}`
    for filename in ${filelist}
    do
        if [ -f $filename ]; then
			ftime=`stat -c %Y $filename`
            if [ $ftime -lt $LAST_TIME ];then
                echo -e "delete file: $filename"
                rm -rf $filename
	    fi
	elif [ -d $filename ];then
	    cd $filename
            FILE_PATH=`pwd`
            get_all_files
            cd ..
	fi

    done
}

echo -e "Now check log in path of '$FILE_PATH'"
cd $FILE_PATH
get_all_files
echo -e "Done!"

